<?php

/**
 * Class: KL_InlineSpoiler_Listener_Formatter
 *
 * Extend XenForo_BbCode_Formatter_Base with
 * KL_InlineSpoiler_BbCode_Formatter_Base
 *
 * Created:			31/03/2016
 * Last changed:	--/--/----
 * Change Counter:	0
 */

class KL_AdvancedTables_Listener_Formatter {
	
    public static function extend($class, array &$extend) {
        $extend[] = 'KL_AdvancedTables_BbCode_Formatter_Base';
    }
	
}