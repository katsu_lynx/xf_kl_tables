<?php

/**
 * KL_Tables_BBCode_Formatter_Tables
 *
 * @author: Nerian
 * @last_edit:    15.09.2015
 */

class KL_AdvancedTables_BBCode_Formatter_Base extends XFCP_KL_AdvancedTables_BBCode_Formatter_Base {

	/**
	 * Get the list of parsable tags and their parsing rules.
	 * Extend with Table stuff
	 *
	 * @return array
	 */
	public function getTags() {
		$tags = parent::getTags();
		
		$tags = array_merge(
			$tags,
			array(
				'table' => array('callback' => array($this, 'renderTagTable')),
				'tr' => array('callback' => array($this, 'renderTagTableRow')),
				'th' => array('callback' => array($this, 'renderTagTableHeadCell')),
				'td' => array('callback' => array($this, 'renderTagTableCell')),
			)
		);
		
		return $tags;
	}
	
	
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter
     * @return: string
     */
    public static function renderTagTable(array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter) {
        $tag = self::_resolveChildren($tag);
        
        $content = '<div class="baseTableContainer"><table class="baseTable '.$tag['option'].'"><tbody>'.$formatter->renderSubTree($tag['children'], $rendererStates).'</tbody></table></div>'; 
        
        return $content;
    }
    
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter
     * @return: string
     */
    public static function renderTagTableCell(array $tag, array $rendererStates, XenForo_BBCode_Formatter_Base $formatter, $tagSyntax = 'td') {
        $keys = array_keys($tag['children']);
		if (!$keys)
		{
			return '<'.$tagSyntax.'></'.$tagSyntax.'>';
		}

		$first = reset($keys);
		$last = end($keys);

		if (is_string($tag['children'][$first]))
		{
			$tag['children'][$first] = ltrim($tag['children'][$first]);
		}
		if (is_string($tag['children'][$last]))
		{
			$tag['children'][$last] = rtrim($tag['children'][$last]);
		}

		$content = $formatter->renderSubTree($tag['children'], $rendererStates); 
        
        return '<'.$tagSyntax.' class="'.$tag['option'].'">'.$content.'</'.$tagSyntax.'>';
    }
    
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter
     * @return: string
     */
    public static function renderTagTableHeadCell(array $tag, array $rendererStates, XenForo_BBCode_Formatter_Base $formatter) {
        return self::renderTagTableCell($tag, $rendererStates, $formatter, 'th');
    }
    
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag, array $rendererStates, XenForo_BbCode_Formatter_Base $formatter
     * @return: string
     */
    public static function renderTagTableRow(array $tag, array $rendererStates, XenForo_BBCode_Formatter_Base $formatter) {
        if(self::_findTableCells($tag)) {
            $tag = self::_resolveChildren($tag, array('td', 'th'));
            return '<tr class="'.$tag['option'].'">'.$formatter->renderSubTree($tag['children'],$rendererStates).'</tr>';   
        }
        else {
            return '<tr class="'.$tag['option'].'"><td>'.$formatter->renderSubTree($tag['children'],$rendererStates).'</td></tr>'; 
        }
    }
    
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag, array $resolveArray
     * @return: array
     */
    private static function _resolveChildren($tag, $resolveArray = array('td','tr','th','th')) {
        if(!in_array($tag['tag'], $resolveArray)) {
            foreach($tag['children'] as $key => $child) {
                if(is_string($child)) {
                    unset($tag['children'][$key]);
                }
                else if (is_array($child)) {
                    $tag['children'][$key] = self::_resolveChildren($child);   
                }
            }
        }
        
        return $tag;
    }
    
    /* 
     * @last_edit:	15.09.2015
     * @params: array $tag
     * @return: boolean
     */
    private static function _findTableCells($tag) {
        $return = false;
        if(is_array($tag)) {
            if(in_array($tag['tag'], array('td','th'))) {
                return true;
            }
            else if($tag['tag'] === 'table') {
                $return = false;   
            }
            else {
                foreach($tag['children'] as $child) {
                    $return = $return || self::_findTableCells($child);   
                }
            }
        }
        
        return $return;
    }
    
}